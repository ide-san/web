const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackInlineSVGPlugin = require('html-webpack-inline-svg-plugin');

const isProduction = process.env.NODE_ENV === 'production';

// main config
const config = {
  mode: isProduction ? 'production' : 'development',

  entry: {},

  output: {
    path: path.resolve(__dirname, 'public'),
    filename: '[name].js',
  },

  resolve: {
    extensions: ['.js'],
  },

  module: {
    rules: [
      /*
      {
        test: /\.js$/,
        loader: 'babel-loader',
        options: {
          presets: [
            ['@babel/preset-env', {
              targets: {
                browsers: ['> 1%', 'last 2 versions']
              }
            }]
          ]
        },
        exclude: path.resolve(__dirname, 'node_modules')
      },
      */
      {
        test: /\.s[ac]ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          { loader: 'css-loader' },
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: () => {
                  const plugins = [
                    require('autoprefixer')(),
                  ];
                  if (isProduction) {
                    plugins.push(require('cssnano')({ preset: 'default' }));
                  }
                  return plugins;
                },
              },
            },
          },
          { loader: 'sass-loader' },
        ],
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: {
              minimize: isProduction,
            },
          },
        ],
      },
      {
        test: /\.(ttf|eot|woff|woff2)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'fonts',
          },
        },
      },
      {
        test: /\.(jpg|png|svg)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'img',
            esModule: false,
          },
        },
      },
    ],
  },

  optimization: {
    minimizer: [
      new TerserJSPlugin({}),
      new OptimizeCSSAssetsPlugin({}),
    ],
  },

  plugins: [
    new MiniCssExtractPlugin(),

    // HtmlWebpackPlugin
    new HtmlWebpackPlugin({
      template: './src/index.ejs',
      inject: false,
      minify: isProduction,
    }),
    new HtmlWebpackInlineSVGPlugin(),
  ],
};

// entries
config.entry = {
  main: './src/main.scss',
};

module.exports = config;
