module.exports = {
  root: true,

  env: {
    node: true,
  },

  extends: [
    'semistandard',
  ],

  rules: {
    'comma-dangle': ['error', 'always-multiline'],
  },
};
